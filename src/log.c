#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "log.h"
#include "libmegapixels.h"

static int loglevel = 0;

void
init_log(int level)
{
	loglevel = level;
	char *debuglevel = getenv("LIBMEGAPIXELS_DEBUG");
	if (debuglevel != NULL) {
		char *end;
		long env_level = strtol(debuglevel, &end, 10);
		loglevel = (int) env_level;
	}
}

void
libmegapixels_loglevel(int level)
{
	loglevel = level;
}

void
log_error(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "[libmegapixels] ");
	vfprintf(stderr, fmt, args);
	va_end(args);
}

void
log_debug(const char *fmt, ...)
{
	if (loglevel < 2) {
		return;
	}
	va_list args;
	va_start(args, fmt);
	fprintf(stderr, "[libmegapixels] ");
	vfprintf(stderr, fmt, args);
	va_end(args);
}