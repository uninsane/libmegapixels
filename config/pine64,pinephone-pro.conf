Version = 1;
Make: "PINE64";
Model: "PinePhone Pro";

Rear: {
    SensorDriver: "imx258";
    BridgeDriver: "rkisp1";
    FlashPath: "/sys/class/leds/white:flash";

    Modes: (
        {
            Width: 4208;
            Height: 3120;
            Rate: 30;
            Format: "RGGB8";
            Rotate: 270;
            FocalLength: 3.33;
            FNumber: 3.0;

            Pipeline: (
                {Type: "Link", From: "imx258", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
                {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
                {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
                {Type: "Mode", Entity: "imx258", Format: "RGGB10P"},
                {Type: "Mode", Entity: "rkisp1_csi"},
                {Type: "Mode", Entity: "rkisp1_isp"},
                {Type: "Mode", Entity: "rkisp1_isp", Pad: 2, Format: "RGGB8"},
                {Type: "Crop", Entity: "rkisp1_isp"}, # Cropped by default
                {Type: "Crop", Entity: "rkisp1_isp", Pad: 2}, # Cropped by default
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1}
            );
        },
        # { # Doesn't work well in Megapixels yet
        #     Width: 4208;
        #     Height: 3120;
        #     Rate: 30;
        #     Format: "RGGB10";
        #     Rotate: 270;
        #     FocalLength: 3.33;
        #     FNumber: 3.0;

        #     Pipeline: (
        #         {Type: "Link", From: "imx258", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
        #         {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
        #         {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
        #         {Type: "Mode", Entity: "imx258", Format: "RGGB10P"},
        #         {Type: "Mode", Entity: "rkisp1_csi"},
        #         {Type: "Mode", Entity: "rkisp1_isp"},
        #         {Type: "Mode", Entity: "rkisp1_isp", Pad: 2, Format: "RGGB10"},
        #         {Type: "Crop", Entity: "rkisp1_isp"}, # Cropped by default
        #         {Type: "Crop", Entity: "rkisp1_isp", Pad: 2}, # Cropped by default
        #         {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
        #         {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1}
        #     );
        # },
        {
            Width: 1048;
            Height: 780;
            Rate: 30;
            Format: "RGGB8";
            Rotate: 270;
            FocalLength: 3.33;
            FNumber: 3.0;

            Pipeline: (
                {Type: "Link", From: "imx258", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
                {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
                {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
                {Type: "Mode", Entity: "imx258", Format: "RGGB10P"},
                {Type: "Mode", Entity: "rkisp1_csi"},
                {Type: "Mode", Entity: "rkisp1_isp"},
                {Type: "Mode", Entity: "rkisp1_isp", Pad: 2, Format: "RGGB8"},
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1},
                {Type: "Crop", Entity: "rkisp1_isp"},
                {Type: "Crop", Entity: "rkisp1_isp", Pad: 2},
                {Type: "Crop", Entity: "rkisp1_resizer_mainpath"}
            );
        },
    );
};

Front: {
    SensorDriver: "ov8858";
    BridgeDriver: "rkisp1";

    Modes: (
        {
            Width: 3264;
            Height: 2448;
            Rate: 30;
            Format: "BGGR8";
            Rotate: 90;
            FocalLength: 3.33;
            FNumber: 3.0;

            Pipeline: (
                {Type: "Link", From: "ov8858", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
                {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
                {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
                {Type: "Mode", Entity: "ov8858", Format: "BGGR10"},
                {Type: "Mode", Entity: "rkisp1_csi"},
                {Type: "Mode", Entity: "rkisp1_isp"},
                {Type: "Mode", Entity: "rkisp1_isp", Pad: 2, Format: "BGGR8"},
                {Type: "Crop", Entity: "rkisp1_isp"}, # Cropped by default
                {Type: "Crop", Entity: "rkisp1_isp", Pad: 2}, # Cropped by default
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1}
            );
        },
        # { # Doesn't work well in Megapixels yet
        #     Width: 3264;
        #     Height: 2448;
        #     Rate: 30;
        #     Format: "BGGR10";
        #     Rotate: 90;
        #     FocalLength: 3.33;
        #     FNumber: 3.0;

        #     Pipeline: (
        #         {Type: "Link", From: "ov8858", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
        #         {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
        #         {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
        #         {Type: "Mode", Entity: "ov8858", Format: "BGGR10"},
        #         {Type: "Mode", Entity: "rkisp1_csi"},
        #         {Type: "Mode", Entity: "rkisp1_isp"},
        #         {Type: "Mode", Entity: "rkisp1_isp", Pad: 2},
        #         {Type: "Crop", Entity: "rkisp1_isp"}, # Cropped by default
        #         {Type: "Crop", Entity: "rkisp1_isp", Pad: 2}, # Cropped by default
        #         {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
        #         {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1}
        #     );
        # },
        {
            Width: 1048;
            Height: 780;
            Rate: 30;
            Format: "BGGR8";
            Rotate: 90;
            FocalLength: 3.33;
            FNumber: 3.0;

            Pipeline: (
                {Type: "Link", From: "ov8858", FromPad: 0, To: "rkisp1_csi", ToPad: 0},
                {Type: "Link", From: "rkisp1_csi", FromPad: 1, To: "rkisp1_isp", ToPad: 0},
                {Type: "Link", From: "rkisp1_isp", FromPad: 2, To: "rkisp1_resizer_mainpath", ToPad: 0},
                {Type: "Mode", Entity: "ov8858", Format: "BGGR10", Width: 1632, Height: 1224},
                {Type: "Mode", Entity: "rkisp1_csi"},
                {Type: "Mode", Entity: "rkisp1_isp"},
                {Type: "Mode", Entity: "rkisp1_isp", Pad: 2, Format: "BGGR8", Width: 1048, Height: 780},
                {Type: "Crop", Entity: "rkisp1_isp"}, # Cropped by default
                {Type: "Crop", Entity: "rkisp1_isp", Pad: 2}, # Cropped by default
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath"},
                {Type: "Mode", Entity: "rkisp1_resizer_mainpath", Pad: 1}
            );
        }
    );
};